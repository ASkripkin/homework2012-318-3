#include "test_graph.h"
#include <fstream>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>


CPPUNIT_TEST_SUITE_REGISTRATION(test_graph);

void test_graph::setUp()
{
}

void test_graph::tearDown()
{
}


void test_graph::test_AAT()
{
    ofstream outp ("../.nodes.txt");
    if(!inp.is_open()){cerr << "File not found";}
    outp << "3" << endl;
    outp << "4" << endl;
    outp << "5" << endl;
    outp << "1" << endl;
    outp << "6" << endl;
    outp << "7" << endl;
    outp.close();

    outp.open ("../.nets.txt");
    if(!inp.is_open()){cerr << "File not found";}
    outp << "0 1 1" << endl;
    outp << "0 2 3" << endl;
    outp << "1 3 6" << endl;
    outp << "2 3 5" << endl;
    outp << "2 4 7" << endl;
    outp << "3 5 4" << endl;
    outp << "4 5 2" << endl;
    outp.close();

    outp.open ("../.aux.txt");
    if(!inp.is_open()){cerr << "File not found";}
    outp << "5 32" << endl;
    outp.close();


    int temp_AAT[6], compare_AAT[6];
    graph a;
    a.slack_make();

    list<node>::iterator p;
    for (int i = 0; i < 6; i++)
    {
        p = a.graf[i].begin();
        temp_AAT[i] = p->AAT;
    }

    compare_AAT[0] = 3;
    compare_AAT[1] = 8;
    compare_AAT[2] = 11;
    compare_AAT[3] = 17;
    compare_AAT[4] = 24;
    compare_AAT[5] = 33;


    CPPUNIT_ASSERT(temp_AAT == compare_AAT);
}

void test_graph::test_RAT()
{
    ofstream outp ("../.nodes.txt");
    if(!inp.is_open()){cerr << "File not found";}
    outp << "3" << endl;
    outp << "4" << endl;
    outp << "5" << endl;
    outp << "1" << endl;
    outp << "6" << endl;
    outp << "7" << endl;
    outp.close();

    outp.open ("../.nets.txt");
    if(!inp.is_open()){cerr << "File not found";}
    outp << "0 1 1" << endl;
    outp << "0 2 3" << endl;
    outp << "1 3 6" << endl;
    outp << "2 3 5" << endl;
    outp << "2 4 7" << endl;
    outp << "3 5 4" << endl;
    outp << "4 5 2" << endl;
    outp.close();

    outp.open ("../.aux.txt");
    if(!inp.is_open()){cerr << "File not found";}
    outp << "5 32" << endl;
    outp.close();


    int temp_RAT[6], compare_RAT[6];
    graph a;
    a.slack_make();

    list<node>::iterator p;
    for (int i = 0; i < 6; i++)
    {
        p = a.graf[i].begin();
        temp_RAT[i] = p->RAT;
    }

    compare_RAT[0] = 2;
    compare_RAT[1] = 14;
    compare_RAT[2] = 10;
    compare_RAT[3] = 21;
    compare_RAT[4] = 23;
    compare_RAT[5] = 32;


    CPPUNIT_ASSERT(temp_RAT == compare_RAT);
}


void test_graph::test_slack()
{
    ofstream outp ("../.nodes.txt");
    if(!inp.is_open()){cerr << "File not found";}
    outp << "3" << endl;
    outp << "4" << endl;
    outp << "5" << endl;
    outp << "1" << endl;
    outp << "6" << endl;
    outp << "7" << endl;
    outp.close();

    outp.open ("../.nets.txt");
    if(!inp.is_open()){cerr << "File not found";}
    outp << "0 1 1" << endl;
    outp << "0 2 3" << endl;
    outp << "1 3 6" << endl;
    outp << "2 3 5" << endl;
    outp << "2 4 7" << endl;
    outp << "3 5 4" << endl;
    outp << "4 5 2" << endl;
    outp.close();

    outp.open ("../.aux.txt");
    if(!inp.is_open()){cerr << "File not found";}
    outp << "5 32" << endl;
    outp.close();


    int temp_slack[6], compare_slack[6];
    graph a;
    a.slack_make();

    list<node>::iterator p;
    for (int i = 0; i < 6; i++)
    {
        p = a.graf[i].begin();
        temp_slack[i] = p->slack;
    }

    compare_slack[0] = -1;
    compare_slack[1] = 6;
    compare_slack[2] = -1;
    compare_slack[3] = 4;
    compare_slack[4] = -1;
    compare_slack[5] = -1;


    CPPUNIT_ASSERT(temp_slack == compare_slack);
}


int main()
{
    CppUnit::TextUi::TestRunner runner;
    CppUnit::TestFactoryRegistry &registry
    = CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest(registry.makeTest());
    runner.run();
    return 0;
}
