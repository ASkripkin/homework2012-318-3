/*��������� ����������� �� �������� ���������,
  ��� ��������� ��������� ������: ����� ������,
  ����� � �������. ����� ��������� ��������
  ��������� ��� �������� ���������, ����� � ������
  ������ �������� "../" �� ���� ������ ".nodes.txt"
  "../.nodes.txt" */

#include <iostream>
#include <fstream>
#include <time.h>
#include <stdlib.h>

using namespace std;

int maxn (int vertex, int aux)
{
    int k = 0, j1, j2;
    for (j1 = 0; j1 < aux; j1++);
    for (j2 = j1; j2 < vertex; j2++)
    {
        k += j2;
    }
    return k;
}

int make_int_from_string (string s)
{
    int i = 1, result = s[0] - '0';
    while (s[i])
    {
        result *= 10;
        result += s[i] - '0';
        i++;
    }
    return result;
}

int main(int argc, char *argv[])
{
    int vertex = -1, connects = -1, finishes = -1;
    int i = 0, j = 0, k = 0, max_con, temp_v;
    double temp;
    string s;

    if (argc > 1)
    {
        s = argv[1];
        vertex = make_int_from_string(s);
    }
    if (argc > 2)
    {
        s = argv[2];
        connects = make_int_from_string(s);
    }
    if (argc > 3)
    {
        s = argv[3];
        finishes = make_int_from_string(s);
    }

    if (vertex <= 0)
    {
        cout<<endl<<
        "Please, input number of vertex"<<endl;
        cin>>vertex;
    }

    if (connects <= 0)
    {
        cout<<connects<<endl<<
        "Please, input number of connects"<<endl;
        cin>>vertex;
    }

    if (finishes <= 0)
    {
        cout<<finishes<<endl<<
        "Please, input number of finish vertex"<<endl;
        cin>>vertex;
    }

    if (vertex - connects > 1)
    {
        cout<<"Too much vertex"<<endl;
        return 1;
    }

    if (connects > maxn(vertex, finishes))
    {
        cout<<"Too much connects"<<endl;
        return 1;
    }

    srand((unsigned)time(NULL));

    ofstream outp (".nodes.txt");
    if(!outp.is_open()){cerr<<"File not found"; return 1;}
    for (i = 0; i < vertex; i++)
    {
        temp = rand()%800;
        temp /= 100;
        outp<<temp<<endl;
    }
    outp.close();

    outp.open(".aux.txt");
    if(!outp.is_open()){cerr<<"File not found"; return 1;}
    for (i = vertex - finishes; i < vertex; i++)
    {
        temp = ((400 * vertex - 200) + rand() % (800 * vertex - 400));
        temp /= 100;
        outp<<i<<" "<<temp<<endl;
    }
    outp.close();

    outp.open(".nets.txt");
    if(!outp.is_open()){cerr<<"File not found"; return 1;}
    max_con = connects - vertex + 1;
    temp_v = vertex;

    for (i = 0; i < vertex - finishes; i++)
    {
        temp = rand()%800;
        temp /= 100;
        outp<<i<<" "<<i+1<<" "<<temp<<endl;
        if (i == vertex - finishes - 1)
        {
            for (j=0; j < finishes - 1; j++)
            {
                temp = rand()%800;
                temp /= 100;
                outp<<i<<" "<<i+2+j<<" "<<temp<<endl;
            }
        }
        else
        {
            k = rand() % ((max_con / temp_v + 1) * 2);
            if (k > vertex - i - 2) k = vertex - i - 2;
            else
            {
                j = max_con - maxn(vertex - i - 2, finishes);
                if (k < j) k = j;
            }

            j = i + 2;
            while ((j < vertex) && (k > 0))
            {
                if (vertex - j == k) {

                    for (j = vertex - k; j < vertex; j++)
                    {
                        temp = rand()%800;
                        temp /= 100;
                        outp<<i<<" "<<j<<" "<<temp<<endl;
                        max_con--;
                        k--;
                    }
                                      }
                else
                if (rand() % 2 == 1)
                {
                    temp = rand()%800;
                    temp /= 100;
                    outp<<i<<" "<<j<<" "<<temp<<endl;
                    max_con--;
                    k--;
                }
                j++;
            }
        }
        temp_v--;
    }
    outp.close();



    return 0;

}

