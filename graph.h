#ifndef GRAPH_H
#define GRAPH_H
#include <vector>
#include <list>
#include <iostream>
#include <fstream>

using namespace std;

/**
 *This structure consists of index number of element, it's
 *delay, delay of connection with other element, it's AAT,
 *RAT and slack, and boolean variable, that means direction
 *of connection with other element.
 */


struct node
{
    int number;
    double delay_node;
    double delay_connect;
    double slack;
    double AAT;
    double RAT;
    bool in;
};

/**
 * This class consists of functions that counts slacks of
 * elements in graph. There is helping program that generate
 * graph with preassigned number of vertexes, connectings
 * and out vertexes. Helping program started from main
 * function of this program.
 * @author  Andrew Skripkin
 * @version 1.0
 * @since   1.0
 */


class graph
{
public:

    /**
     * Constructor builds graf from three files that consist
     * number of vertexes, connectings and out vertexes.
     */
    graph();

    /**
     * This function output graf with delays.
     */
    void out_graph();

    /**
     * This function counts slacks in graph, from this function
     * started functions that counts AAT and RAT of elements.
     */
    void slack_make();

    vector < list <node> > graf;

protected:
    /**
     * This function counts AAT of element with number num.
     * Starts from slack_make.
     * @param int num
     * @return double AAT
     */
    double AAT_make(int num);

    /**
     * This function counts RAT of element with number num. RAT_high
     * is RAT from one of elements to what directs one of connections.
     * Starts from slack_make.
     * @param int num
     * @param double RAT_high
     * @return double RAT
     */
    double RAT_make(int num, double RAT_high);

    int size;
};



#endif // GRAF_H
