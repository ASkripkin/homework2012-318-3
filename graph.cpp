#include "graph.h"
#include <fstream>

graph::graph()
{
    double del_n, del_c, v_in, v_out;
    int i = 0;
    node *node_temp;
    list<node>::iterator p;
    list <node> *list_temp;

    //������ �� ����� .nodes �������� � �����
    ifstream inp ("../.nodes.txt");
    if(!inp.is_open()){cerr << "File not found";}
    //������������� �����
    while (inp >> del_n)
    {
        node_temp = new node;
        list_temp = new list <node>;
        node_temp->number = i;
        node_temp->delay_node = del_n;
        node_temp->delay_connect = 0; //������ �������������
        node_temp->slack = 0; //������ �������������
        node_temp->AAT = -1; //������ �������������
        node_temp->RAT = -65535; //������ �������������
        node_temp->in = false;
        list_temp->push_back(*node_temp);
        graf.push_back(*list_temp);
        i++;
        delete (node_temp);
        delete (list_temp);
    }

    size = i;

    inp.close();

    //������ �� ����� .nets �������� � ������
    inp.open("../.nets.txt");
    if(!inp.is_open()){cerr << "File not found";}
    //������������� �����
    while (inp >> v_out)
    {
        inp >> v_in;
        inp >> del_c;
        node_temp = new node;
        list_temp = new list <node>;
        p = graf[v_in].begin();
        del_n = p->delay_node;
        node_temp->number = v_in;
        node_temp->delay_node = del_n;
        node_temp->delay_connect = del_c;
        node_temp->slack = 0; //������ �������������
        node_temp->AAT = -1; //������ �������������
        node_temp->RAT = -65535; //������ �������������
        node_temp->in = false;
        *list_temp = graf[v_out];
        list_temp->push_back(*node_temp);
        graf[v_out] = *list_temp;
        delete (list_temp);
        i = v_in;
        list_temp = new list <node>;
        p = graf[v_out].begin();
        del_n = p->delay_node;
        node_temp->delay_node = del_n;
        node_temp->number = v_out;
        node_temp->in = true;
        *list_temp = graf[i];
        list_temp->push_back(*node_temp);
        graf[i] = *list_temp;
        delete (node_temp);
        delete (list_temp);
    }
    inp.close();
}

void graph::out_graph()
{
    list<node>::iterator p;
    for (int i = 0; i < size; i++)
    {
        p = graf[i].begin();
        cout << i << "(" << p->delay_node << ") : ";
        p++;
        while (p != graf[i].end())
        {
            cout << p->number << "(" << p->delay_connect << ")  ";
            p++;
        }
        cout << endl;
    }
}

double graph::AAT_make(int num)
{
    double delay = 0, delay2 = 0, AAT = 0;
    list<node>::iterator p;
    p = graf[num].begin();
    if (p->AAT != -1) return p->AAT;
    delay = p->delay_node;
    p++;
    AAT = delay;
    while (p != graf[num].end())
    {
        if (p->in == 1)
        {
            delay2 = AAT_make(p->number) + p->delay_connect + delay;
            if (AAT < delay2) AAT = delay2;
        }
        p++;
    }
    p = graf[num].begin();
    p->AAT = AAT;
    return AAT;
}

double graph::RAT_make(int num, double RAT_high)
{
    double delay = 0;
    list<node>::iterator p;
    list<node>::iterator q;
    p = graf[num].begin();
    p->RAT = RAT_high;
    delay = p->delay_node;
    p = graf[num].end();
    p--;
    while (p != graf[num].begin())
    {
        if (p->in == 1)
        {
            q = graf[p->number].begin();
            if ((q->RAT > RAT_high - delay - p->delay_connect)||(q->RAT == -65535))
                //������ ������� - ���� q->RAT �� ���������������
                RAT_make(p->number, RAT_high - delay - p->delay_connect);
        }
        p--;
    }
    return RAT_high;
}

void graph::slack_make()
{
    int vertex;
    double temp, RAT;
    list<node>::iterator p;
    list<node>::iterator q;

    //��� ������ �������� ������� �� ����� .aux ��������� ������ AAT
    ifstream inp ("../.aux.txt");
    if(!inp.is_open()){cerr << "File not found";}
    while (inp >> vertex)
    {
        inp >> temp;
        p = graf[vertex].begin();
        q = p;
        p++;
        while (p != graf[vertex].end())
        {
            if (p->AAT == -1) temp = AAT_make(p->number)
                    + p->delay_connect + q->delay_node;
            if (q->AAT < temp) q->AAT = temp;
            p++;
        }
    }

    inp.close();

    //��� ������ �������� ������� �� ����� .aux ��������� ������ RAT
    inp.open("../.aux.txt");
    if(!inp.is_open()){cerr << "File not found";}

    while (inp >> vertex)
    {
        p = graf[vertex].begin();
        inp >> p->RAT;
        RAT = p->RAT;
        temp = p->delay_node;
        p++;
        while (p != graf[vertex].end())
        {
            q = graf[p->number].begin();
            if ((q->RAT > RAT - temp - p->delay_connect)||(q->RAT == -65535))
                //������ ������� - ���� q->RAT �� ���������������
                RAT_make(p->number, RAT - temp - p->delay_connect);
            p++;
        }
    }

    for (int i = 0; i < size; i++)
    {
        p = graf[i].begin();
        p->slack = p->RAT - p->AAT;
    }

    ofstream outp ("../.slacks.txt");

    for (int i = 0; i < size; i++)
    {
        p = graf[i].begin();
        outp << p->slack << endl;
    }

    outp.close();

    outp.open("../.result.txt");
    bool ok = 0;

    inp.close();
    inp.open("../.slacks.txt");

    for (int i = 0; i < size; i++)
    {
        inp >> temp;
        if (temp < 0)
        {
            if (!ok) {outp << "1" << endl; ok++;}
            outp << i << "  ";
        }
    }
    if (!ok) outp << "0";
    outp.close();
    inp.close();
}
