#include <iostream>
#include <../graph.h>
#include <fstream>
#include <time.h>
#include <windows.h>
using namespace std;

string make_string_from_int(int a)
{
    if (a == 0) return "0";
    string sqwe = "";
    char c;
    while (a>0)
    {
        c = a%10 + '0';
        sqwe = c + sqwe;
        a /= 10;
    }
    return sqwe;
}

int main()
{
    clock_t start;
    double duration;
    string m;
    const char* m2;// = m.c_str();
    std::cout.setf(std::ios:: fixed);
    std::cout.precision(2);
    m = "..\\helper\\debug\\helper.exe ";
    m += make_string_from_int(35) + " ";
    m += make_string_from_int(77) + " ";
    m += make_string_from_int(1);
    //������ ����� - ���-�� ������, ������ - �����, ������ - �������� ������
    m2 = m.c_str();
    system(m2);
    start = clock();
    graph a1;
    a1.slack_make();
    a1.out_graph();
    duration = (double)(clock() - start) / CLOCKS_PER_SEC;
    cout << endl << "progress time = " << duration * 1000;
    Sleep(10000);
    return 0;
}
