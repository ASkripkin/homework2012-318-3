#ifndef TEST_GRAPH_H
#define TEST_GRAPH_H

#include <cppunit/extensions/HelperMacros.h>
#include "graph.h"


class test_graph : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(test_graph);
    CPPUNIT_TEST(test_AAT);
    CPPUNIT_TEST(test_RAT);
    CPPUNIT_TEST(test_slack);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp();
    void tearDown();

protected:
    void test_AAT();
    void test_RAT();
    void test_slack();

private:

};
