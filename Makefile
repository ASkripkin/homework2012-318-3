all: slacks profile testall

slacks: debug/graph.o debug/main.o
	g++ debug/graph.o debug/main.o -o debug/graf
	
graph.o: graph.h graph.cpp
	g++ -c graph.cpp
	
main.o: main.cpp
	g++ -c main.cpp
	
profile: helper/debug/helper.o 
	g++ helper/debug/helper.o -o helper/debug/helper

helper/debug/helper.o: helper/helper.cpp
	g++ -c helper/helper.cpp

testall: debug/test_graph.o 
	g++ debug/test_graph.o -o debug/test_graf
	
test_graph.o: test_graph.h test_graph.cpp
	g++ -c test_graph.cpp

clean:
	rm -f main
	rm -f profile
	rm -f testall
	rm -f *.o
